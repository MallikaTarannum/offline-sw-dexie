import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import {HttpClientModule} from '@angular/common/http';
import {NgxIndexedDBModule} from 'ngx-indexed-db';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmployeeComponent } from './employee/employee.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
   AppRoutingModule,
   ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
   NgxIndexedDBModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
