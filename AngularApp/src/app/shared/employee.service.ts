import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Subject} from 'rxjs';
import Dexie from 'dexie';
import {Router} from '@angular/router';
import TurtleDB from 'turtledb';

import { Employee } from './employee.model';
import {OnlineOfflineService} from './online-offline.service';
import { stringify } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  selectedEmployee: Employee;
  employees: Employee[];
  private db:any;
  readonly baseURL="http://localhost:3000/employees";

   constructor(private http:HttpClient, private readonly onlineOfflineService:OnlineOfflineService,
    private router:Router){
      this.createDatabase(),
     this.registerToEvent(onlineOfflineService);
     this.listenToEvents(onlineOfflineService);
   }
  
   

   private registerToEvent(onlineOfflineService:OnlineOfflineService){
     onlineOfflineService.connectionChanged.subscribe(online=>{
       if(online){
         console.log('went online');
         console.log('sending all stored items');

         this.sendItemsFromIndexedDB();
       }else {
        console.log('went offline, storing in indexdb');
      }
     })
   }

   private listenToEvents(onlineOfflineService){
     onlineOfflineService.connectionChanged.subscribe(online=>{
       if(online){
         console.log('Went online');
        // this.sendItemsToDelete();
       }
       else{
         console.log('went offline');
       }
     })
   }



//   postEmployee(emp:Employee){
// return this.http.post(this.baseURL,emp);
//   }

postEmployee(emp:Employee){
  if(!this.onlineOfflineService.isOnline){this.addToIndexedDB(emp);}

  return this.http.post(this.baseURL,emp);
    }

  getEmployeeList(){
    return this.http.get(this.baseURL);
  }
       
  putEmployee(emp: Employee) {
    return this.http.put(this.baseURL + `/${emp._id}`, emp);
  }

  deleteEmployee(_id: string) {
    return this.http.delete(this.baseURL + `/${_id}`);
  }
// public mydb:any;

// createDatabase(){
//   const mydb = new TurtleDB('TEST');

//   mydb.setRemote('http://127.0.0.1:3000');

//   mydb.create({_id:this.selectedEmployee._id,
//   name:this.selectedEmployee.name,
// position:this.selectedEmployee.position,
// office:this.selectedEmployee.office,
// salary:this.selectedEmployee.salary});

// mydb.sync();
// }

// sendToIDB(emp:Employee){
  
// }


  // private db:any;
  // private donedb:any;

//create Indexed db
//  createIndexedDatabase(){
//   this.db=new Dexie("TestDatabase");
// this.db.version(1).stores({
//   emp:"_id,name,position,office,salary"
// });
// this.donedb.open().catch(function(err){
//   console.log(err.stack || err);
// });
//  }

// private createDoneDatabase(){
//   this.donedb=new Dexie("DoneDatabase");
// this.donedb.version(1).stores({
//   emp:'_id'
// });
// this.donedb.open().catch(function(err){
// console.log(err.stack || err);
// })

// }


//  dbConfig:DBConfig={
// name:'MyDB',
// version:1,
// objectStoresMeta:[{
//   store:'emp',
//   storeConfig:{keyPath:'_id',autoIncrement:false},
//   storeSchema:[
//     {name:'name',keypath:'name',options:{unique:false}},
//     {name:'position',keypath:'position',options:{unique:false}},
//     {name:'office',keypath:'office',options:{unique:false}},
//     {name:'salary',keypath:'salary',options:{unique:false}},
//   ]
// }]
// };


private createDatabase(){
  this.db=new Dexie('TestDB');
  this.db.version(1).stores({
    employees:'name,position,office,salary'
  });
}

private async addToIndexedDB(emp:Employee){
  this.db.employees
  .add(emp)
  .then(async ()=>{
    const allItems:Employee[]=await this.db.employees.toArray();
    console.log('saved in db',allItems);
  })
  .catch(e=>{
    console.log('Error' + e.stack || e);
  });
}

private async sendItemsFromIndexedDB() {
  const allItems:Employee[]=await this.db.employees.toArray();

  this.bulkEmp(allItems).subscribe(res=>{
    console.log(res);
    this.db.employees.clear();
  })
  // allItems.forEach((item:Employee)=>{
  //   this.db.employees.delete(item._id).then(()=>{
  //     console.log(`item $(item._id) sent and deleted locally`);
  //   });
  // });
}

addEmp(emp:Employee){
//emp["_id"]=null;

if(!this.onlineOfflineService.isOnline){
  this.addToIndexedDB(emp);
}
else{
  this.postEmployee(emp)
  // .subscribe(res=>{
  //   console.log(res);
  //   this.router.navigateByUrl('/',{skipLocationChange:true}).then(()=>{
  //     this.router.navigate(["/employees"])
  //   });
  // });
}
}

public bulkEmp(emp:any){
  const headers=new HttpHeaders().set('Content-Type','application/json');

  //posting
  return this.http.post("http://localhost:3000/employees",JSON.stringify(emp),{headers:headers});
}

}